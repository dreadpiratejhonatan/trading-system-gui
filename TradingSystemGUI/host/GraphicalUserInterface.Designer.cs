﻿namespace host {
    partial class GraphicalUserInterface {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.panelNewOrdersPanel = new System.Windows.Forms.Panel();
            this.buttonSell = new System.Windows.Forms.Button();
            this.buttonBuy = new System.Windows.Forms.Button();
            this.labelNewOrders = new System.Windows.Forms.Label();
            this.textBoxPx = new System.Windows.Forms.TextBox();
            this.textBoxQty = new System.Windows.Forms.TextBox();
            this.labelPx = new System.Windows.Forms.Label();
            this.labelQty = new System.Windows.Forms.Label();
            this.labelInstrument = new System.Windows.Forms.Label();
            this.comboBoxInstrument = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grvWorkingOrders = new System.Windows.Forms.DataGridView();
            this.labelWorkingOrdersManagement = new System.Windows.Forms.Label();
            this.labelBD = new System.Windows.Forms.Label();
            this.panelMarketData = new System.Windows.Forms.Panel();
            this.comboBoxBDFind = new System.Windows.Forms.ComboBox();
            this.buttonBDFind = new System.Windows.Forms.Button();
            this.grvBD = new System.Windows.Forms.DataGridView();
            this.colBidSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBidPx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAskPx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAskSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelMarketDataTopOfBook = new System.Windows.Forms.Panel();
            this.comboBoxTOBAdd = new System.Windows.Forms.ComboBox();
            this.buttonTOBAdd = new System.Windows.Forms.Button();
            this.grvTOB = new System.Windows.Forms.DataGridView();
            this.colTOBSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTOBBidSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTOBBidPx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTOBAskPx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTOBAskSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelTOB = new System.Windows.Forms.Label();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSide = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFilled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRemaining = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colObservations = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelNewOrdersPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvWorkingOrders)).BeginInit();
            this.panelMarketData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvBD)).BeginInit();
            this.panelMarketDataTopOfBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvTOB)).BeginInit();
            this.SuspendLayout();
            // 
            // panelNewOrdersPanel
            // 
            this.panelNewOrdersPanel.BackColor = System.Drawing.Color.LightBlue;
            this.panelNewOrdersPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelNewOrdersPanel.Controls.Add(this.buttonSell);
            this.panelNewOrdersPanel.Controls.Add(this.buttonBuy);
            this.panelNewOrdersPanel.Controls.Add(this.labelNewOrders);
            this.panelNewOrdersPanel.Controls.Add(this.textBoxPx);
            this.panelNewOrdersPanel.Controls.Add(this.textBoxQty);
            this.panelNewOrdersPanel.Controls.Add(this.labelPx);
            this.panelNewOrdersPanel.Controls.Add(this.labelQty);
            this.panelNewOrdersPanel.Controls.Add(this.labelInstrument);
            this.panelNewOrdersPanel.Controls.Add(this.comboBoxInstrument);
            this.panelNewOrdersPanel.Location = new System.Drawing.Point(12, 12);
            this.panelNewOrdersPanel.Name = "panelNewOrdersPanel";
            this.panelNewOrdersPanel.Size = new System.Drawing.Size(259, 171);
            this.panelNewOrdersPanel.TabIndex = 0;
            // 
            // buttonSell
            // 
            this.buttonSell.BackColor = System.Drawing.Color.MistyRose;
            this.buttonSell.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.buttonSell.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSell.ForeColor = System.Drawing.Color.DarkRed;
            this.buttonSell.Location = new System.Drawing.Point(132, 117);
            this.buttonSell.Name = "buttonSell";
            this.buttonSell.Size = new System.Drawing.Size(115, 38);
            this.buttonSell.TabIndex = 5;
            this.buttonSell.Text = "SELL";
            this.buttonSell.UseVisualStyleBackColor = false;
            this.buttonSell.Click += new System.EventHandler(this.ButtonSell_Click);
            // 
            // buttonBuy
            // 
            this.buttonBuy.BackColor = System.Drawing.Color.PaleGreen;
            this.buttonBuy.FlatAppearance.BorderColor = System.Drawing.Color.DarkGreen;
            this.buttonBuy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBuy.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonBuy.Location = new System.Drawing.Point(11, 117);
            this.buttonBuy.Name = "buttonBuy";
            this.buttonBuy.Size = new System.Drawing.Size(115, 38);
            this.buttonBuy.TabIndex = 4;
            this.buttonBuy.Text = "BUY";
            this.buttonBuy.UseVisualStyleBackColor = false;
            this.buttonBuy.Click += new System.EventHandler(this.ButtonBuy_Click);
            // 
            // labelNewOrders
            // 
            this.labelNewOrders.AutoSize = true;
            this.labelNewOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOrders.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelNewOrders.Location = new System.Drawing.Point(11, 3);
            this.labelNewOrders.Name = "labelNewOrders";
            this.labelNewOrders.Size = new System.Drawing.Size(136, 13);
            this.labelNewOrders.TabIndex = 1;
            this.labelNewOrders.Text = "NEW ORDERS PANEL";
            // 
            // textBoxPx
            // 
            this.textBoxPx.Location = new System.Drawing.Point(132, 88);
            this.textBoxPx.Name = "textBoxPx";
            this.textBoxPx.Size = new System.Drawing.Size(115, 20);
            this.textBoxPx.TabIndex = 3;
            // 
            // textBoxQty
            // 
            this.textBoxQty.Location = new System.Drawing.Point(11, 88);
            this.textBoxQty.Name = "textBoxQty";
            this.textBoxQty.Size = new System.Drawing.Size(115, 20);
            this.textBoxQty.TabIndex = 2;
            // 
            // labelPx
            // 
            this.labelPx.AutoSize = true;
            this.labelPx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPx.Location = new System.Drawing.Point(132, 72);
            this.labelPx.Name = "labelPx";
            this.labelPx.Size = new System.Drawing.Size(36, 13);
            this.labelPx.TabIndex = 1;
            this.labelPx.Text = "Price";
            // 
            // labelQty
            // 
            this.labelQty.AutoSize = true;
            this.labelQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQty.Location = new System.Drawing.Point(11, 72);
            this.labelQty.Name = "labelQty";
            this.labelQty.Size = new System.Drawing.Size(54, 13);
            this.labelQty.TabIndex = 1;
            this.labelQty.Text = "Quantity";
            // 
            // labelInstrument
            // 
            this.labelInstrument.AutoSize = true;
            this.labelInstrument.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInstrument.Location = new System.Drawing.Point(11, 27);
            this.labelInstrument.Name = "labelInstrument";
            this.labelInstrument.Size = new System.Drawing.Size(66, 13);
            this.labelInstrument.TabIndex = 1;
            this.labelInstrument.Text = "Instrument";
            // 
            // comboBoxInstrument
            // 
            this.comboBoxInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInstrument.FormattingEnabled = true;
            this.comboBoxInstrument.Location = new System.Drawing.Point(11, 43);
            this.comboBoxInstrument.Name = "comboBoxInstrument";
            this.comboBoxInstrument.Size = new System.Drawing.Size(236, 21);
            this.comboBoxInstrument.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.grvWorkingOrders);
            this.panel1.Controls.Add(this.labelWorkingOrdersManagement);
            this.panel1.Location = new System.Drawing.Point(277, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(818, 171);
            this.panel1.TabIndex = 0;
            // 
            // grvWorkingOrders
            // 
            this.grvWorkingOrders.AllowUserToAddRows = false;
            this.grvWorkingOrders.AllowUserToDeleteRows = false;
            this.grvWorkingOrders.AllowUserToResizeColumns = false;
            this.grvWorkingOrders.AllowUserToResizeRows = false;
            this.grvWorkingOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grvWorkingOrders.BackgroundColor = System.Drawing.Color.White;
            this.grvWorkingOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvWorkingOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colStatus,
            this.colSymbol,
            this.colSide,
            this.colPx,
            this.colQty,
            this.colFilled,
            this.colRemaining,
            this.colObservations});
            this.grvWorkingOrders.Location = new System.Drawing.Point(14, 19);
            this.grvWorkingOrders.Name = "grvWorkingOrders";
            this.grvWorkingOrders.ReadOnly = true;
            this.grvWorkingOrders.RowHeadersVisible = false;
            this.grvWorkingOrders.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grvWorkingOrders.Size = new System.Drawing.Size(788, 136);
            this.grvWorkingOrders.TabIndex = 1;
            this.grvWorkingOrders.TabStop = false;
            this.grvWorkingOrders.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrvWorkingOrders_CellClick);
            // 
            // labelWorkingOrdersManagement
            // 
            this.labelWorkingOrdersManagement.AutoSize = true;
            this.labelWorkingOrdersManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWorkingOrdersManagement.ForeColor = System.Drawing.Color.DarkGreen;
            this.labelWorkingOrdersManagement.Location = new System.Drawing.Point(11, 3);
            this.labelWorkingOrdersManagement.Name = "labelWorkingOrdersManagement";
            this.labelWorkingOrdersManagement.Size = new System.Drawing.Size(258, 13);
            this.labelWorkingOrdersManagement.TabIndex = 1;
            this.labelWorkingOrdersManagement.Text = "WORKING ORDERS MANAGEMENT PANEL";
            // 
            // labelBD
            // 
            this.labelBD.AutoSize = true;
            this.labelBD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBD.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.labelBD.Location = new System.Drawing.Point(11, 3);
            this.labelBD.Name = "labelBD";
            this.labelBD.Size = new System.Drawing.Size(223, 13);
            this.labelBD.TabIndex = 1;
            this.labelBD.Text = "MARKET DATA BOOK DEPTH PANEL";
            // 
            // panelMarketData
            // 
            this.panelMarketData.BackColor = System.Drawing.Color.LightYellow;
            this.panelMarketData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMarketData.Controls.Add(this.comboBoxBDFind);
            this.panelMarketData.Controls.Add(this.buttonBDFind);
            this.panelMarketData.Controls.Add(this.grvBD);
            this.panelMarketData.Controls.Add(this.labelBD);
            this.panelMarketData.Location = new System.Drawing.Point(557, 190);
            this.panelMarketData.Name = "panelMarketData";
            this.panelMarketData.Size = new System.Drawing.Size(539, 255);
            this.panelMarketData.TabIndex = 0;
            // 
            // comboBoxBDFind
            // 
            this.comboBoxBDFind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBDFind.FormattingEnabled = true;
            this.comboBoxBDFind.Location = new System.Drawing.Point(11, 20);
            this.comboBoxBDFind.Name = "comboBoxBDFind";
            this.comboBoxBDFind.Size = new System.Drawing.Size(400, 21);
            this.comboBoxBDFind.TabIndex = 5;
            this.comboBoxBDFind.TabStop = false;
            // 
            // buttonBDFind
            // 
            this.buttonBDFind.Location = new System.Drawing.Point(417, 21);
            this.buttonBDFind.Name = "buttonBDFind";
            this.buttonBDFind.Size = new System.Drawing.Size(105, 21);
            this.buttonBDFind.TabIndex = 4;
            this.buttonBDFind.TabStop = false;
            this.buttonBDFind.Text = "Find";
            this.buttonBDFind.UseVisualStyleBackColor = true;
            // 
            // grvBD
            // 
            this.grvBD.AllowUserToAddRows = false;
            this.grvBD.AllowUserToDeleteRows = false;
            this.grvBD.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grvBD.BackgroundColor = System.Drawing.Color.White;
            this.grvBD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvBD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colBidSize,
            this.colBidPx,
            this.colAskPx,
            this.colAskSize});
            this.grvBD.Location = new System.Drawing.Point(11, 45);
            this.grvBD.Name = "grvBD";
            this.grvBD.ReadOnly = true;
            this.grvBD.RowHeadersVisible = false;
            this.grvBD.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grvBD.Size = new System.Drawing.Size(511, 199);
            this.grvBD.TabIndex = 3;
            this.grvBD.TabStop = false;
            // 
            // colBidSize
            // 
            this.colBidSize.HeaderText = "Bid Size";
            this.colBidSize.Name = "colBidSize";
            this.colBidSize.ReadOnly = true;
            // 
            // colBidPx
            // 
            this.colBidPx.HeaderText = "Bid Px";
            this.colBidPx.Name = "colBidPx";
            this.colBidPx.ReadOnly = true;
            // 
            // colAskPx
            // 
            this.colAskPx.HeaderText = "Ask Px";
            this.colAskPx.Name = "colAskPx";
            this.colAskPx.ReadOnly = true;
            // 
            // colAskSize
            // 
            this.colAskSize.HeaderText = "Ask Size";
            this.colAskSize.Name = "colAskSize";
            this.colAskSize.ReadOnly = true;
            // 
            // panelMarketDataTopOfBook
            // 
            this.panelMarketDataTopOfBook.BackColor = System.Drawing.Color.LightYellow;
            this.panelMarketDataTopOfBook.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMarketDataTopOfBook.Controls.Add(this.comboBoxTOBAdd);
            this.panelMarketDataTopOfBook.Controls.Add(this.buttonTOBAdd);
            this.panelMarketDataTopOfBook.Controls.Add(this.grvTOB);
            this.panelMarketDataTopOfBook.Controls.Add(this.labelTOB);
            this.panelMarketDataTopOfBook.Location = new System.Drawing.Point(12, 190);
            this.panelMarketDataTopOfBook.Name = "panelMarketDataTopOfBook";
            this.panelMarketDataTopOfBook.Size = new System.Drawing.Size(539, 255);
            this.panelMarketDataTopOfBook.TabIndex = 0;
            // 
            // comboBoxTOBAdd
            // 
            this.comboBoxTOBAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTOBAdd.FormattingEnabled = true;
            this.comboBoxTOBAdd.Location = new System.Drawing.Point(11, 21);
            this.comboBoxTOBAdd.Name = "comboBoxTOBAdd";
            this.comboBoxTOBAdd.Size = new System.Drawing.Size(401, 21);
            this.comboBoxTOBAdd.TabIndex = 7;
            this.comboBoxTOBAdd.TabStop = false;
            // 
            // buttonTOBAdd
            // 
            this.buttonTOBAdd.Location = new System.Drawing.Point(418, 21);
            this.buttonTOBAdd.Name = "buttonTOBAdd";
            this.buttonTOBAdd.Size = new System.Drawing.Size(105, 21);
            this.buttonTOBAdd.TabIndex = 6;
            this.buttonTOBAdd.TabStop = false;
            this.buttonTOBAdd.Text = "Add";
            this.buttonTOBAdd.UseVisualStyleBackColor = true;
            // 
            // grvTOB
            // 
            this.grvTOB.AllowUserToAddRows = false;
            this.grvTOB.AllowUserToDeleteRows = false;
            this.grvTOB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grvTOB.BackgroundColor = System.Drawing.Color.White;
            this.grvTOB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvTOB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTOBSymbol,
            this.colTOBBidSize,
            this.colTOBBidPx,
            this.colTOBAskPx,
            this.colTOBAskSize});
            this.grvTOB.Location = new System.Drawing.Point(11, 45);
            this.grvTOB.Name = "grvTOB";
            this.grvTOB.ReadOnly = true;
            this.grvTOB.RowHeadersVisible = false;
            this.grvTOB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.grvTOB.Size = new System.Drawing.Size(512, 199);
            this.grvTOB.TabIndex = 2;
            this.grvTOB.TabStop = false;
            // 
            // colTOBSymbol
            // 
            this.colTOBSymbol.HeaderText = "Symbol";
            this.colTOBSymbol.Name = "colTOBSymbol";
            this.colTOBSymbol.ReadOnly = true;
            // 
            // colTOBBidSize
            // 
            this.colTOBBidSize.HeaderText = "Bid Size";
            this.colTOBBidSize.Name = "colTOBBidSize";
            this.colTOBBidSize.ReadOnly = true;
            // 
            // colTOBBidPx
            // 
            this.colTOBBidPx.HeaderText = "Bid Px";
            this.colTOBBidPx.Name = "colTOBBidPx";
            this.colTOBBidPx.ReadOnly = true;
            // 
            // colTOBAskPx
            // 
            this.colTOBAskPx.HeaderText = "Ask Px";
            this.colTOBAskPx.Name = "colTOBAskPx";
            this.colTOBAskPx.ReadOnly = true;
            // 
            // colTOBAskSize
            // 
            this.colTOBAskSize.HeaderText = "Ask Size";
            this.colTOBAskSize.Name = "colTOBAskSize";
            this.colTOBAskSize.ReadOnly = true;
            // 
            // labelTOB
            // 
            this.labelTOB.AutoSize = true;
            this.labelTOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTOB.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.labelTOB.Location = new System.Drawing.Point(11, 3);
            this.labelTOB.Name = "labelTOB";
            this.labelTOB.Size = new System.Drawing.Size(226, 13);
            this.labelTOB.TabIndex = 1;
            this.labelTOB.Text = "MARKET DATA TOP OF BOOK PANEL";
            // 
            // colStatus
            // 
            this.colStatus.DataPropertyName = "Status";
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            // 
            // colSymbol
            // 
            this.colSymbol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.colSymbol.DataPropertyName = "Symbol";
            this.colSymbol.HeaderText = "Symbol";
            this.colSymbol.Name = "colSymbol";
            this.colSymbol.ReadOnly = true;
            this.colSymbol.Width = 66;
            // 
            // colSide
            // 
            this.colSide.DataPropertyName = "Side";
            this.colSide.HeaderText = "Side";
            this.colSide.Name = "colSide";
            this.colSide.ReadOnly = true;
            // 
            // colPx
            // 
            this.colPx.DataPropertyName = "Px";
            this.colPx.HeaderText = "Px";
            this.colPx.Name = "colPx";
            this.colPx.ReadOnly = true;
            // 
            // colQty
            // 
            this.colQty.DataPropertyName = "Qty";
            this.colQty.HeaderText = "Qty";
            this.colQty.Name = "colQty";
            this.colQty.ReadOnly = true;
            // 
            // colFilled
            // 
            this.colFilled.DataPropertyName = "Filled";
            this.colFilled.HeaderText = "Filled";
            this.colFilled.Name = "colFilled";
            this.colFilled.ReadOnly = true;
            // 
            // colRemaining
            // 
            this.colRemaining.DataPropertyName = "Remaining";
            this.colRemaining.HeaderText = "Remaining";
            this.colRemaining.Name = "colRemaining";
            this.colRemaining.ReadOnly = true;
            // 
            // colObservations
            // 
            this.colObservations.DataPropertyName = "Observations";
            this.colObservations.HeaderText = "Observations";
            this.colObservations.Name = "colObservations";
            this.colObservations.ReadOnly = true;
            // 
            // GraphicalUserInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 455);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelMarketDataTopOfBook);
            this.Controls.Add(this.panelMarketData);
            this.Controls.Add(this.panelNewOrdersPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "GraphicalUserInterface";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trading System Client";
            this.Load += new System.EventHandler(this.GraphicalUserInterface_Load);
            this.panelNewOrdersPanel.ResumeLayout(false);
            this.panelNewOrdersPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvWorkingOrders)).EndInit();
            this.panelMarketData.ResumeLayout(false);
            this.panelMarketData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvBD)).EndInit();
            this.panelMarketDataTopOfBook.ResumeLayout(false);
            this.panelMarketDataTopOfBook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvTOB)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelNewOrdersPanel;
        private System.Windows.Forms.Button buttonBuy;
        private System.Windows.Forms.Label labelNewOrders;
        private System.Windows.Forms.TextBox textBoxPx;
        private System.Windows.Forms.TextBox textBoxQty;
        private System.Windows.Forms.Label labelPx;
        private System.Windows.Forms.Label labelQty;
        private System.Windows.Forms.Label labelInstrument;
        private System.Windows.Forms.ComboBox comboBoxInstrument;
        private System.Windows.Forms.Button buttonSell;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView grvWorkingOrders;
        private System.Windows.Forms.Label labelWorkingOrdersManagement;
        private System.Windows.Forms.Label labelBD;
        private System.Windows.Forms.Panel panelMarketData;
        private System.Windows.Forms.Panel panelMarketDataTopOfBook;
        private System.Windows.Forms.Label labelTOB;
        private System.Windows.Forms.DataGridView grvBD;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBidSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBidPx;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAskPx;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAskSize;
        private System.Windows.Forms.ComboBox comboBoxBDFind;
        private System.Windows.Forms.Button buttonBDFind;
        private System.Windows.Forms.DataGridView grvTOB;
        private System.Windows.Forms.ComboBox comboBoxTOBAdd;
        private System.Windows.Forms.Button buttonTOBAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTOBSymbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTOBBidSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTOBBidPx;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTOBAskPx;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTOBAskSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSymbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSide;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPx;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFilled;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRemaining;
        private System.Windows.Forms.DataGridViewTextBoxColumn colObservations;
    }
}

