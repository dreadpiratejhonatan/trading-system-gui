﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace host {
    class InstrumentsConfig {
        public InstrumentsConfig() {

        }

        public static Dictionary<string, string> GetInstruments() {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Config\instruments.json");
            using (StreamReader r = new StreamReader(path)) {
                string json = r.ReadToEnd();
                Dictionary<string, string> instruments = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                return instruments;
            }
        }
    }
}
