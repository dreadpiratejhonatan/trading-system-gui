﻿namespace host {
    using host.Entities;
    using host.Views;
    using NLog;
    using QuickFix;
    using QuickFix.Fields;
    using QuickFix.FIX44;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;
    using SocketInitiator = QuickFix.Transport.SocketInitiator;

    public partial class GraphicalUserInterface : Form {
        static Logger _logger = LogManager.GetCurrentClassLogger();
        EditOrderForm editOrderForm;
        private ClientApplication app = new ClientApplication();
        SocketInitiator _initiator;
        BindingList<WorkingOrder> workingOrders;
        Dictionary<string, WorkingOrder> ids;
        Dictionary<string, string> instruments;

        public GraphicalUserInterface() {
            workingOrders = new BindingList<WorkingOrder>();
            ids = new Dictionary<string, WorkingOrder>();
            editOrderForm = new EditOrderForm();
            instruments = new Dictionary<string, string>();
            InitializeInitiator();
            InitializeComponent();
        }

        private void GraphicalUserInterface_Load(object sender, EventArgs e) {
            try {
                BindDataSources();
                SetupView();
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        private void BindDataSources() {
            try {
                grvWorkingOrders.AutoGenerateColumns = false;
                grvWorkingOrders.DataSource = workingOrders;
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        private void SetupView() {
            try {
                DataGridViewImageColumn editBtn = new DataGridViewImageColumn();
                editBtn.HeaderText = "Edit";
                editBtn.Image = Properties.Resources.edit;

                DataGridViewImageColumn cancelBtn = new DataGridViewImageColumn();
                cancelBtn.HeaderText = "Cancel";
                cancelBtn.Image = Properties.Resources.clear;

                grvWorkingOrders.Columns.Add(editBtn);
                grvWorkingOrders.Columns.Add(cancelBtn);

                foreach (var kv in InstrumentsConfig.GetInstruments()) {
                    instruments[kv.Key] = kv.Value;
                    comboBoxInstrument.Items.Add(kv.Key);
                    comboBoxBDFind.Items.Add(kv.Key);
                    comboBoxTOBAdd.Items.Add(kv.Key);
                }
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        private void InitializeInitiator() {
            IMessageStoreFactory storeFactory = new FileStoreFactory(ClientApplicationConfig.Instance.GetSessionSettings());
            ILogFactory logFactory = new ScreenLogFactory(ClientApplicationConfig.Instance.GetSessionSettings());
            _initiator = new SocketInitiator(app,
                                            storeFactory,
                                            ClientApplicationConfig.Instance.GetSessionSettings(),
                                            logFactory);
            _initiator.Start();
        }

        private void ButtonBuy_Click(object sender, EventArgs e) {
            try {
                if (comboBoxInstrument == null ||
                    textBoxQty == null ||
                    textBoxPx == null ||
                    string.IsNullOrEmpty(comboBoxInstrument.Text) ||
                    string.IsNullOrEmpty(textBoxQty.Text) ||
                    string.IsNullOrEmpty(textBoxPx.Text)) {
                    return;
                }
                var clOrdId = Guid.NewGuid().ToString();
                var instrument = comboBoxInstrument.Text;
                var secId = instruments[instrument];
                var qty = textBoxQty.Text;
                var px = textBoxPx.Text;

                NewOrderSingle order = new NewOrderSingle();
                order.ClOrdID = new ClOrdID(clOrdId);
                order.Side = new Side(Side.BUY);
                order.Symbol = new Symbol(instrument);
                order.SecurityID = new SecurityID(secId);
                order.OrderQty = new OrderQty(Convert.ToDecimal(qty));
                order.Price = new Price(Convert.ToDecimal(px));
                app.SendMessage(order);

                WorkingOrder workingOrder = new WorkingOrder();
                workingOrder.ClOrdId = order.ClOrdID.ToString();
                workingOrder.OrigClOrdId = string.Empty;
                workingOrder.Side = char.Parse(order.Side.ToString());
                workingOrder.Symbol = order.Symbol.ToString();
                workingOrder.SecurityID = order.SecurityID.ToString();
                workingOrder.Qty = long.Parse(order.OrderQty.ToString());
                workingOrder.Px = decimal.Parse(order.Price.ToString());
                workingOrders.Add(workingOrder);
                ids.Add(clOrdId, workingOrder);

                ClearNewOrderFields();
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        private void ButtonSell_Click(object sender, EventArgs e) {
            if (comboBoxInstrument == null ||
                    textBoxQty == null ||
                    textBoxPx == null ||
                    string.IsNullOrEmpty(comboBoxInstrument.Text) ||
                    string.IsNullOrEmpty(textBoxQty.Text) ||
                    string.IsNullOrEmpty(textBoxPx.Text)) {
                return;
            }
            var clOrdId = Guid.NewGuid().ToString();
            var instrument = comboBoxInstrument.Text;
            var secId = instruments[instrument];
            var qty = textBoxQty.Text;
            var px = textBoxPx.Text;

            NewOrderSingle order = new NewOrderSingle();
            order.ClOrdID = new ClOrdID(clOrdId);
            order.Side = new Side(Side.SELL);
            order.Symbol = new Symbol(instrument);
            order.SecurityID = new SecurityID(secId);
            order.OrderQty = new OrderQty(Convert.ToDecimal(qty));
            order.Price = new Price(Convert.ToDecimal(px));
            app.SendMessage(order);

            WorkingOrder workingOrder = new WorkingOrder();
            workingOrder.ClOrdId = order.ClOrdID.ToString();
            workingOrder.OrigClOrdId = string.Empty;
            workingOrder.Side = char.Parse(order.Side.ToString());
            workingOrder.Symbol = order.Symbol.ToString();
            workingOrder.SecurityID = order.SecurityID.ToString();
            workingOrder.Qty = long.Parse(order.OrderQty.ToString());
            workingOrder.Px = decimal.Parse(order.Price.ToString());
            workingOrders.Add(workingOrder);
            ids.Add(clOrdId, workingOrder);

            ClearNewOrderFields();
        }

        private void ClearNewOrderFields() {
            textBoxQty.Clear();
            textBoxPx.Clear();
        }

        private void GrvWorkingOrders_CellClick(object sender, DataGridViewCellEventArgs e) {
            try {
                if (e.ColumnIndex == 8) {
                    var workingOrder = workingOrders[e.RowIndex];
                    editOrderForm.SetNewQty(workingOrder.Qty);
                    editOrderForm.SetNewPx(workingOrder.Px);
                    editOrderForm.ShowDialog();
                    if (editOrderForm.confirmed &&
                        !editOrderForm.canceled) {
                        if (!editOrderForm.GetNewQty(out var newQty)) {
                            return;
                        }
                        if (!editOrderForm.GetNewPx(out var newPx)) {
                            return;
                        }
                        editOrderForm.ClearEditOrderFields();
                        workingOrder.OrigClOrdId = workingOrder.ClOrdId;
                        workingOrder.ClOrdId = Guid.NewGuid().ToString();
                        workingOrder.Qty = newQty.Value;
                        workingOrder.Px = newPx.Value;
                        grvWorkingOrders.Update();
                        grvWorkingOrders.Refresh();
                        ReplaceOrder(workingOrder);
                    }
                }
                else if (e.ColumnIndex == 9) {
                    var order = workingOrders[e.RowIndex];
                    if (MessageBox.Show($"ORDER DETAILS: {order.ToString()}", "Cancel order?", MessageBoxButtons.OKCancel) == DialogResult.OK) {
                        workingOrders.RemoveAt(e.RowIndex);
                        CancelOrder(order);
                    }
                }
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        private void ReplaceOrder(WorkingOrder order) {
            try {

            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        private void CancelOrder(WorkingOrder order) {
            try {

            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }
    }
}
