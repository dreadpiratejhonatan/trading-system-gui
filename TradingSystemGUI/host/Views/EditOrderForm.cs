﻿using NLog;

namespace host.Views {
    using System;
    using System.Windows.Forms;

    public partial class EditOrderForm : Form {
        static Logger _logger = LogManager.GetCurrentClassLogger();
        public bool confirmed = false;
        public bool canceled = false;

        public EditOrderForm() {
            InitializeComponent();
        }

        public void SetNewQty(long newQty) {
            try {
                textBoxQty.Text = newQty.ToString();
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        public void SetNewPx(decimal newPx) {
            try {
                textBoxPx.Text = newPx.ToString();
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        public bool GetNewQty(out long? newQty) {
            if (string.IsNullOrEmpty(textBoxQty.Text)) {
                newQty = null;
                return false;
            }
            if (long.TryParse(textBoxQty.Text, out var value)) {
                newQty = value;
                return true;
            };
            newQty = null;
            return false;
        }

        public bool GetNewPx(out decimal? newPx) {
            if (string.IsNullOrEmpty(textBoxPx.Text)) {
                newPx = null;
                return false;
            }
            if (decimal.TryParse(textBoxPx.Text, out var value)) {
                newPx = value;
                return true;
            };
            newPx = null;
            return false;
        }

        private void ButtonConfirm_Click(object sender, EventArgs e) {
            confirmed = true;
            canceled = false;
            this.Close();
        }

        private void ButtonCancel_Click(object sender, EventArgs e) {
            confirmed = false;
            canceled = true;
            this.Close();
        }

        internal void ClearEditOrderFields() {
            textBoxQty.Clear();
            textBoxPx.Clear();
        }

        private void EditOrderForm_Load(object sender, EventArgs e) {
            textBoxQty.Focus();
        }

        private void TextBoxQty_KeyPress(object sender, KeyPressEventArgs e) {
            if (e.KeyChar == (char)Keys.Enter) {
                buttonConfirm.PerformClick();
            }
            else if (e.KeyChar == (char)Keys.Escape) {
                buttonCancel.PerformClick();
            }
        }

        private void TextBoxPx_KeyPress(object sender, KeyPressEventArgs e) {
            if (e.KeyChar == (char)Keys.Enter) {
                buttonConfirm.PerformClick();
            }
            else if (e.KeyChar == (char)Keys.Escape) {
                buttonCancel.PerformClick();
            }
        }
    }
}
