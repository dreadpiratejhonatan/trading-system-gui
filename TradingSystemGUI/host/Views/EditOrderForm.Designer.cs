﻿namespace host.Views {
    partial class EditOrderForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panelEditOrder = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.textBoxPx = new System.Windows.Forms.TextBox();
            this.textBoxQty = new System.Windows.Forms.TextBox();
            this.labelPx = new System.Windows.Forms.Label();
            this.labelQty = new System.Windows.Forms.Label();
            this.panelEditOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelEditOrder
            // 
            this.panelEditOrder.BackColor = System.Drawing.Color.LightBlue;
            this.panelEditOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEditOrder.Controls.Add(this.buttonCancel);
            this.panelEditOrder.Controls.Add(this.buttonConfirm);
            this.panelEditOrder.Controls.Add(this.textBoxPx);
            this.panelEditOrder.Controls.Add(this.textBoxQty);
            this.panelEditOrder.Controls.Add(this.labelPx);
            this.panelEditOrder.Controls.Add(this.labelQty);
            this.panelEditOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEditOrder.Location = new System.Drawing.Point(0, 0);
            this.panelEditOrder.Name = "panelEditOrder";
            this.panelEditOrder.Size = new System.Drawing.Size(258, 87);
            this.panelEditOrder.TabIndex = 1;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.MistyRose;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.ForeColor = System.Drawing.Color.DarkRed;
            this.buttonCancel.Location = new System.Drawing.Point(132, 48);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(115, 25);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "CANCEL";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Honeydew;
            this.buttonConfirm.FlatAppearance.BorderColor = System.Drawing.Color.DarkGreen;
            this.buttonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConfirm.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonConfirm.Location = new System.Drawing.Point(11, 49);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(115, 25);
            this.buttonConfirm.TabIndex = 3;
            this.buttonConfirm.Text = "CONFIRM";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // textBoxPx
            // 
            this.textBoxPx.Location = new System.Drawing.Point(132, 23);
            this.textBoxPx.Name = "textBoxPx";
            this.textBoxPx.Size = new System.Drawing.Size(115, 20);
            this.textBoxPx.TabIndex = 2;
            this.textBoxPx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxPx_KeyPress);
            // 
            // textBoxQty
            // 
            this.textBoxQty.Location = new System.Drawing.Point(11, 23);
            this.textBoxQty.Name = "textBoxQty";
            this.textBoxQty.Size = new System.Drawing.Size(115, 20);
            this.textBoxQty.TabIndex = 1;
            this.textBoxQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxQty_KeyPress);
            // 
            // labelPx
            // 
            this.labelPx.AutoSize = true;
            this.labelPx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPx.Location = new System.Drawing.Point(132, 7);
            this.labelPx.Name = "labelPx";
            this.labelPx.Size = new System.Drawing.Size(65, 13);
            this.labelPx.TabIndex = 1;
            this.labelPx.Text = "New Price";
            // 
            // labelQty
            // 
            this.labelQty.AutoSize = true;
            this.labelQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQty.Location = new System.Drawing.Point(11, 7);
            this.labelQty.Name = "labelQty";
            this.labelQty.Size = new System.Drawing.Size(83, 13);
            this.labelQty.TabIndex = 1;
            this.labelQty.Text = "New Quantity";
            // 
            // EditOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(258, 87);
            this.Controls.Add(this.panelEditOrder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "EditOrderForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Order";
            this.Load += new System.EventHandler(this.EditOrderForm_Load);
            this.panelEditOrder.ResumeLayout(false);
            this.panelEditOrder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelEditOrder;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.TextBox textBoxPx;
        private System.Windows.Forms.TextBox textBoxQty;
        private System.Windows.Forms.Label labelPx;
        private System.Windows.Forms.Label labelQty;
        private System.Windows.Forms.Button buttonCancel;
    }
}