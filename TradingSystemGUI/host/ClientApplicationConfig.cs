﻿namespace host {
    using QuickFix;
    using System;

    sealed class ClientApplicationConfig {
        static ClientApplicationConfig instance = new ClientApplicationConfig();
        static readonly object syncLock = new object();
        SessionSettings Settings { get; }

        private ClientApplicationConfig() {
            Settings = new SessionSettings($"{AppDomain.CurrentDomain.BaseDirectory}\\Config\\clientApplication.cfg");
        }

        public static ClientApplicationConfig Instance {
            get {
                lock (syncLock) {
                    return instance;
                }
            }
        }

        public SessionSettings GetSessionSettings() {
            return Settings;
        }
    }
}
