﻿using NLog;
using QuickFix;
using QuickFix.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace host {
    class ClientApplication : MessageCracker, IApplication {
        static Logger _logger = LogManager.GetCurrentClassLogger();
        Session _sn = null;

        public void SendMessage(Message msg) {
            if (_sn == null ||
                msg == null) {
                return;
            }
            _sn.Send(msg);
        }

        public void OnCreate(SessionID sessionID) {
            _sn = Session.LookupSession(sessionID);
        }

        public void OnLogon(SessionID sessionID) {
            _logger.Info($"LOGON: {sessionID.ToString()}");
        }

        public void OnLogout(SessionID sessionID) {
            _logger.Info($"LOGOUT: {sessionID.ToString()}");
        }

        public void FromAdmin(Message message, SessionID sessionID) {

        }

        public void ToAdmin(Message message, SessionID sessionID) {

        }

        public void FromApp(Message msg, SessionID id) {
            try {
                _logger.Info($"IN: {msg.ToString()}");
                Crack(msg, id);
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        public void ToApp(Message message, SessionID sessionID) {
            try {

            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }

        public void OnMessage(QuickFix.FIX44.ExecutionReport executionReport, SessionID sessionID) {
            try {
                _logger.Info($"Execution report received: {executionReport.ToString()}");
            }
            catch (Exception ex) { _logger.Info($"{ex.Message} | {ex.StackTrace}"); }
        }
    }
}
