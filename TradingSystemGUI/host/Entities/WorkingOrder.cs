﻿namespace host.Entities {
    class WorkingOrder {

        public WorkingOrder() {

        }

        public string ClOrdId { get; set; }
        public string OrigClOrdId { get; set; }
        public string Status { get; set; }
        public string Symbol { get; set; }
        public string SecurityID { get; set; }

        private char _Side;
        public char Side {
            get {
                return _Side == '1' ? 'B' : 'S';
            }
            set {
                _Side = value;
            }
        }

        public decimal Px { get; set; }
        public long Qty { get; set; }
        public long Filled { get; set; }
        public long Remaining { get; set; }
        public string Observations { get; set; }

        public override string ToString() {
            return $"Symbol: {Symbol}, " +
                   $"Side: {Side}, " +
                   $"Px: {Px}, " +
                   $"Qty: {Qty}, " +
                   $"Filled: {Filled}, " +
                   $"Remaining: {Remaining}";
        }
    }
}
